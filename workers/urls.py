from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import TemplateView

from taskmanager.views import set_task, get_result

urlpatterns = [
    url(r'^task/', set_task),
    url(r'^result/', get_result),
    url(r'^django-rq/', include('django_rq.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'', TemplateView.as_view(template_name='taskmanager/test.html'))
]
