# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('taskmanager', '0002_auto_20150913_1347'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='job_id',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='task',
            name='message',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='task',
            name='result',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
