from django.db import models
from django.contrib.auth.models import User

class Task(models.Model):
    message = models.CharField(max_length=255, null=False)
    priority = models.PositiveSmallIntegerField(null=False)
    created_on = models.DateTimeField(auto_now_add=True)
    job_id = models.CharField(max_length=255)
    result = models.CharField(max_length=255, blank=True, null=True)
    owner = models.ForeignKey(User, null=True)

    def __unicode__(self):
        if self.owner:
            return '%s: %s' % (self.owner.username, self.result)
        else:
            return '%s: %s' % (self.job_id, str(self.created_on))