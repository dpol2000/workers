import re
import json

from django.http import HttpResponse, JsonResponse
from django.conf import settings
from django.contrib.auth import authenticate
from django.views.decorators.http import require_POST, require_GET

import django_rq

from .models import Task
from tasks import work

@require_POST
def set_task(request):
    '''
    Get messages, priority, username, password, check it and create a task. Return tasks ids and messages.
    '''

    data = request.POST # {'priority': '1', 'msg1': 'aaa', 'msg2': 'bbb', 'username': 'test', 'password': 'test'}

    # get priority
    priority = data.get('priority')
    if priority is None:
        return HttpResponse('Error: no priority')

    if priority not in settings.RQ_QUEUES:
        return HttpResponse('Error: no valid priority')

    # get messages
    message_names = re.findall('msg\d', ' '.join(data.keys()))

    if not message_names:
        return HttpResponse('Error: no messages')

    messages = [data[key] for key in message_names]

    # get user
    username = data.get('username')
    password = data.get('password')

    if username is None or password is None:
        return HttpResponse('Error: no username or password specified')

    user = authenticate(username=username, password=password)

    if user is None:
        return HttpResponse('Error: username or password is wrong')

    # connect to rq, set task to queue

    queue = django_rq.get_queue(priority)

    codes = []

    if queue is not None:
        for message in messages:
            job = queue.enqueue(work, priority, message)
            task = Task(job_id=job.id, priority=priority, message=message, owner=user)
            task.save()
            codes.append(job.id)
    else:
        return HttpResponse('Error in getting queue')

    return JsonResponse({'messages': messages, 'codes': codes}, safe=False)


@require_GET
def get_result(request):
    '''
    Get task id, username, password, check it and return result of task execution.
    '''

    data = request.GET # {'code': 'cc24bfa5-0166-44db-a424-133c09565941', 'username': 'test', 'password': 'test'}

    task_id = data.get('code')

    if task_id is None:
        return HttpResponse('No task specified')

    try:
        task = Task.objects.get(job_id=task_id)
    except Task.DoesNotExist:
        task = None

    if task is None:
        return HttpResponse('No valid task')

    # get user
    username = data.get('username')
    password = data.get('password')

    if username is None or password is None:
        return HttpResponse('Error: no username or password specified')

    user = authenticate(username=username, password=password)

    if user is None:
        return HttpResponse('Error: username or password is wrong')
    elif task.owner == user:
        if task.result:
            return HttpResponse(task.result)

        return HttpResponse('wait')
    else:
        return HttpResponse('Error: not this user\'s task')




