from time import sleep, time
from random import randint

from taskmanager.models import Task
from rq import get_current_job

def work(priority, message):
    '''
    :param priority: not in use now
    :param message: not in use now
    :return: result of execution
    '''
    r = randint(10, 180)
    sleep(r)
    job = get_current_job()
    try:
        task = Task.objects.get(job_id=job.id)
    except Task.DoesNotExist:
        task = None

    if task:
        task.result = str(r)
        task.save()
        return 'ok'
    else:
        return 'error'


